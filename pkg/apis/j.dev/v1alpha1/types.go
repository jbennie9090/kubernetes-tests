package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CustCrd struct {
	metav1.TypeMeta
	metav1.ObjectMeta
	Spec CrdSpec
}

type CrdSpec struct {
	Memebers int
	Json     string
}

//+k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CustCrdList struct {
	metav1.TypeMeta
	metav1.ObjectMeta
	Items []CustCrd
}
