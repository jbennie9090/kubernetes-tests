package v1aplha1

//registers our custom resource
import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const groupName = "joey.dev"

var SchemeGroupVersion = schema.GroupVersion{Group: "test.dev", Version: "v1alpha1"}
var SchemeBuilder runtime.SchemeBuilder

func init() {
	SchemeBuilder.Register(addKnownTypes)
}
func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(SchemeGroupVersion, &CustCrd{}, &CustCrdList{})
	metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
	return nil
}
